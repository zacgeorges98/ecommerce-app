import { createClient } from "next-sanity"

import { apiVersion, dataset, projectId, useCdn } from "../env"

export const client = createClient({
  apiVersion,
  dataset,
  projectId,
  useCdn,
  // token:
  //   "skQKX4OwWRRALM4IV5XN7wm3FV2uEPCMlmOJ18XfIew5JZXY51Grgk8JP6DGzm3GcczPSxk1XQUToThyYXlQFNe2eCzSr0CCv8juu9okG5g6G8azHWU8LMZFCxwpF11BUqykVNVTgWHMS3sHw1BWcEY48azNSL4yISlgnrg2SMarhLoFEFWe", NEEDED ONLY FOR SEEDING DATA USE IF UPDATING PRODUCTS
})
