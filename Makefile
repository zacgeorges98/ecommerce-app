ifneq (,$(wildcard ./.env))
    include .env
    export
endif

dev: clean-dev docker-compose-dev 

# Stop and remove the Docker containers for development
clean-dev:
	docker-compose -f docker-compose.dev.yml down
	docker rmi ${DOCKER_IMAGE} -f

# Docker compose for development
docker-compose-dev:
	docker-compose -f docker-compose.dev.yml up -d --build

# Stop and remove the Docker containers
clean:
	docker-compose down
	docker rm

.PHONY: dev clean-dev docker-compose-dev clean
