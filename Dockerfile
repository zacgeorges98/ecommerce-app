# Common Stage
FROM node:18-alpine AS common
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm ci
COPY . .

# Build Stage (for both Development and Production)
FROM common AS build
RUN npm run build

# Base Stage for Development and Production (common settings)
FROM node:18-alpine AS base
RUN apk update && apk upgrade && apk add dumb-init curl && adduser -D nextuser 
WORKDIR /app
COPY --from=build --chown=nextuser:nextuser /app/public ./public
COPY --from=build --chown=nextuser:nextuser /app/.next/standalone ./
COPY --from=build --chown=nextuser:nextuser /app/.next/static ./.next/static
USER nextuser
ENV NEXT_TELEMETRY_DISABLED 1
ENV HOST=0.0.0.0

# Development Stage
FROM base AS development
CMD ["dumb-init","node","server.js"]

# Production Stage
FROM base AS production
ENV NODE_ENV=production
CMD ["dumb-init","node","server.js"]
